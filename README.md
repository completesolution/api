# API

## Clear Express:

#### server.js

    import express from 'express'
    const app = express
    const users = express.Router()

    users.get('/get', (req, res) => res.status(200).json({
        status: /* Custom status DATA */,
        list: [...]
    }))

    app.use(users)

#### client.js

    fetch(withArgs('host:port/users.get', { ...params }), {
        method: 'GET'
    }).then(data => {
        switch (data.status) {
            case /* Custom status DATA */:
                console.log(data.list)
                break
            //...
        }
    })

## With API

#### nodes/node_name.js

    import { OK } from 'http-status'

    export default {
        status_codes: {
            DATA : OK,
            //...
        },
        methods: {
            get: 'GET:/* ...params */'
        }
    }

#### server.js

    import express from 'express'
    import api from '@cs-museum/server'
    const app = express
    const users = express.Router()

    api.users.get((req, res) => res.api.data({
        list: [...]
    }))

    app.use(api.router())

#### client.js

    import api, { handle, DATA } from '@cs-museum/client'

    api.users
        .query({ /* тут можно добавить query */ })
        .body({ /* тут можно добавить body (порядок body и query не важен) */ })
        .get({ /* тут тоже можно добавить query (можно и там и там, замерджится) */ })
        .then(handle({
            DATA: ({ list }) => console.log(list)
        }))
