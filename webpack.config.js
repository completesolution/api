const pathToDist = [__dirname]

module.exports = [
	require('./webpack.client.config.js')(pathToDist, 'web'),
	require('./webpack.client.config.js')(pathToDist, 'node'),
	require('./webpack.server.config.js')(pathToDist)
]
