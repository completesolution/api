const path = require('path')

module.exports = (dist) => ({
	mode: 'production',
	entry: './src/server/index.js',
	target: 'node',
	output: {
		path: path.resolve(...dist, 'server'),
		filename: 'index.js',
		libraryTarget: 'umd'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [['env', {
							targets: {
								node: 'current'
							},
							modules: false
						}]],
						plugins: [
							'lodash',
							'transform-object-rest-spread'
						]
					}
				}
			}
		]
	},
	resolve: {
		alias: {
			core: path.resolve(__dirname, 'src', 'core'),
			nodes: path.resolve(__dirname, 'src', 'nodes')
		}
	},
	externals: [
		'lodash',
		/^lodash\/.+$/,
		'express',
		'mongoose',
		'http-status'
	]
})
