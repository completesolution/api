const path = require('path')

module.exports = (dist, target) => ({
	mode: 'production',
	entry: './src/client/index.js',
	target: target,
	output: {
		path: path.resolve(...dist, 'client'),
		filename: `${target}.js`,
		libraryTarget: 'umd'
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [['env', {
							targets: {
								browsers: 'IE >= 11'
							},
							modules: false
						}]],
						plugins: [
							'lodash',
							'transform-object-rest-spread'
						]
					}
				}
			}
		]
	},
	resolve: {
		alias: {
			core: path.resolve(__dirname, 'src', 'core'),
			nodes: path.resolve(__dirname, 'src', 'nodes')
		}
	},
	externals: [
		'lodash',
		/^lodash\/.+$/,
		'axios',
		'http-status'
	]
})
