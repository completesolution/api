import {
	assign, flatMap, fromPairs, has, isUndefined, kebabCase, keys, overArgs,
	reduce, rest, snakeCase, spread, toPairs, toUpper, constant, filter, first,
   flow, identity, includes, join, map, omit, property, replace, split, times,
   trim
} from 'lodash/fp'

import HTTP_METHODS from './http-methods'
import * as HTTP_STATUSES from 'http-status'

const GAG = 'UNDEFINED'

const overEach = (...transformers) =>
	spread(overArgs(rest(identity, 0), transformers))

const dublicate = arg => [arg, arg]

const braces = flow(times(constant('}')), join(' '))
const pass = func => arg => { func(arg); return arg }
const log = pass((...args) => console.log(...args))
const omitName = omit(['name'])

const transformLinesToJSON = (lines) => {
	const { length } = lines
	let [ currLine, nextLine ] = lines
	let json = ''

	for (let i = 1; i < length; i += 1) {
		const [ currIndent, src ] = currLine
		const [ nextIndent ] = nextLine

		if (currIndent === nextIndent) {
			json += `${prepareSingleLine(src)},\n`
		} else if (currIndent < nextIndent) {
			json += `"${src}": {\n`
		} else {
			json += `${prepareSingleLine(src)}\n${braces(currIndent - nextIndent)},\n`
		}

		currLine = nextLine
		nextLine = lines[i + 1]
	}

	const [ lastIndent, lastSrc ] = lines[lines.length - 1]
	json += `${prepareSingleLine(lastSrc)}\n${braces(lastIndent)}`

	return `{\n${json}\n}`
}

const prepareSingleLine = src =>
	src.includes(' ') ?
		`"${src.replace(/\s+/, '":"')}"` :
		`"${src}":"${GAG}"`

const parseHeader = flow(
	split(/\s+/),
	overEach(
		flow(snakeCase, toUpper),
		kebabCase
	)
)

const parseReq = (req = {}) => {
	const result = {}

	if (has('query', req)) {
		result.query = req.query
	}

	return result
}

const convertStatusNameToCode = statusName => {
	if (!has(statusName, HTTP_STATUSES)) {
		throw new Error(`There no HTTP status named ${statusName}`)
	}

	return HTTP_STATUSES[statusName]
}

const createAPIStatuses = ([ httpStatusName, apiStatuses ]) => {
	const statusCode = convertStatusNameToCode(httpStatusName)

	return flow(
		map(replace('~', `_${httpStatusName}`)),
		reduce((config, apiMethodName) => {
			config[apiMethodName] = statusCode
			return config
		}, {})
	)(apiStatuses)
}

const parseStatuses = flow(
	toPairs,
	map(overEach(
		flow(toUpper),
		flow(keys, map(toUpper)),
	)),
	flatMap(createAPIStatuses),
	reduce(assign, {})
)

const parseRes = (res = {}) => {
	const result = {}

	if (has('statuses', res)) {
		result.status_codes = parseStatuses(res.statuses)
	}

	return result
}

const parseMethodBody = (body, httpMethod) => {
	if (isUndefined(body)) {
		return {}
	}

	const res = parseRes(body.res)
	const req = parseReq(body.req)

	return {
		http_method: httpMethod,
		...res,
		...req
	}
}

const assemblyMethod = ([header, body]) => {
	const [ httpMethod, nodeMethod ] = parseHeader(header)

	if (!includes(httpMethod, HTTP_METHODS)) {
		throw new Error(`Unexpected HTTP method ${httpMethod}`)
	}

	if (!nodeMethod) {
		throw new Error(`API node method undefined`)
	}

	return [ nodeMethod, parseMethodBody(body, httpMethod) ]
}

const assemblyMethods = flow(
	toPairs,
	map(assemblyMethod),
	fromPairs
)

const assemblyConfig = src => {
	const { name } = src
	const methods = omitName(src)

	return {
		name,
		config: assemblyMethods(methods)
	}
}

const subtractOffset = ([[ offset ]]) =>
	([ indent, src ]) => [ indent - offset, src ]

const removeIndentOffset = lines => lines.map(subtractOffset(lines))

const normalizeIndent = (line, index, indents) => {
	const [ indent, src ] = line
	if (index === 0) return line
	if (indent < 0) return [ 0, src ]
	if (indent - indents[index - 1] > 1) return [ indents[index - 1] + 1, src ]
	return line
}

const normalizeIndents = indents => indents.map(normalizeIndent)

const dublicateWith = (transformerA, transformerB) => arg =>
	[transformerA(arg), transformerB(arg)]

const calcIndent = flow(
	line => line.match(/^\t*/),
	first,
	property('length')
)

const concatSrc = (strs, ...values) => {
	strs = strs.flatMap(str => [str, ''])
	values = values.flatMap(value => ['', value.toString()])
	values.push('', '')
	return strs.map((str, index) => str + values[index]).join('')
}

const transpile = flow(
	concatSrc,
	replace(/#/g, 'name '),
	replace(/:/g, ' '),
	split('\n'),
	filter(line => !/^\s*$/.test(line)),
	map(flow(
		dublicate,
		overEach(calcIndent, trim)
	)),
	removeIndentOffset,
	normalizeIndents,
	transformLinesToJSON,
   JSON.parse,
	assemblyConfig
)

export default transpile
