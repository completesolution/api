import { toString, toNumber, mapValues } from 'lodash'
import { mapKeys, camelCase } from 'lodash/fp'
import { Types } from 'mongoose'

const mapKeysToCamelCase = mapKeys(camelCase)

const typeCnoverters = {
	str: toString,
	num: toNumber,
	oid: Types.ObjectId
}

export default (types, query) => {
	try {
		return mapKeysToCamelCase(mapValues(types, (type, name) => {
			try {
				return typeCnoverters[type](query[name] || '')
			} catch(e) {
				e.__query_field_value__ = query[name]
				return e
			}
		}))
	} catch(e) {
		throw new Error('Something wrong with query', query)
	}
}
