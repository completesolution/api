import express from 'express'
import ServerAPI from '../api'
import ClientAPI from '../../client/api'
import { handle } from '../../client/helpers'
require('isomorphic-fetch')

import {
	OK,
	CREATED,
	UNAUTHORIZED,
	FORBIDDEN,
	NOT_FOUND
} from 'http-status'

const app = express()
const host = 'localhost'
const port = 3000

const serverAPI = new ServerAPI
let clientAPI

serverAPI.plug`
	#Users

	PUT register
		req
			query
				login str
				password str
				name str
				role num
		res
			statuses
				CREATED
					USER_REGISTERED
				FORBIDDEN
					USER_ALREADY_EXISTS

	POST authorize
		req
			query
				login str
				password str
		res
			statuses
				CREATED
					USER_AUTHORIZED
				FORBIDDEN
					USER_ALREADY_EXISTS
				NOT_FOUND
					USER_DOES_NOT_EXIST
				UNAUTHORIZED
					INCORRECT_PASSWORD
`

serverAPI.users
	.authorize((req, res) => {
		res.api.userAuthorized({ users: [1, 2, 3, 4]})
	})
	.register((req, res, next) => {
		req.xData = 'data'
		next()
	}, (req, res) => {
		res.api.userRegistered({
			xData: req.xData
		})
	})

app.use((req, res, next) => {
	next()
})

app.use('/api', serverAPI.router)
let appServer

beforeAll(() => new Promise(resolve => {
	appServer = app.listen(port, async () => {
		console.log(`Testing app listening on port ${port}`)

		clientAPI = new ClientAPI({
			url: { host, port, root: 'api' }
		})

		console.log('Client connecting to server...', clientAPI.initialization)
		await clientAPI.initialization
		console.log('Client connected')
		resolve()
	})
}))

test('connecting client to server serverAPI', done => {
	clientAPI.users
		.authorize({ x: 4 })
		.then(handle({
			USER_AUTHORIZED: data => {
				expect(data.users).toEqual([1, 2, 3, 4])
				done()
			}
		}))
})

test('server chaining methods', done => {
	clientAPI.users
		.register()
		.then(() => done())
})

test('server support middlewares', done => {
	clientAPI.users
		.register()
		.then(handle({
			USER_REGISTERED: data => {
				expect(data.xData).toEqual('data')
				done()
			}
		}))
})

afterAll(() => {
	appServer.close(() => {
		console.log(`Testing app closed on port ${port}`)
	})
})
