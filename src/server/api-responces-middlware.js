import { mapValues, camelCase, flow, isEmpty } from 'lodash'
import { mapKeys, has } from 'lodash/fp'
import transformQueryTypes from './transform-query-types'

const hasBody=has('body')

const createResponceMethod = (http_status, name) => {
	return function(data) {
		return this //this == res
			.status(http_status)
			.json({
				...data,
				http_status,
				api_status: name
			})
	}
}

const createResponceMethods = flow(
	// mapValuesFP(createResponceMethod),
	config => mapValues(config, createResponceMethod),
	mapKeys(camelCase)
)

const bindMethod = context => method => method.bind(context)

const createResponceAPI = (config) => {
	const methods = createResponceMethods(config)
	return (res) => mapValues(methods, bindMethod(res))
}

export default (query, statuses) => {
	const responceAPI = createResponceAPI(statuses)
	return (req, res, next) => {
		if (hasBody(req) && !isEmpty(req.body)) {
			req.query = transformQueryTypes(query, req.body)
		} else {
			req.query = transformQueryTypes(query, req.query)
		}

		res.api = responceAPI(res)
		next()
	}
}
