import { has, each, kebabCase, camelCase } from 'lodash'
import createNode from './create-node'
import { Router } from 'express'
import createClientConnector from './create-client-connector'
import transpile from './transpile'
import bodyParser from 'body-parser'

export default class API {
	constructor() {
		this.init = createClientConnector(this)
		this.nodes = ['init']
		this.config = {}
	}

	plug(...source) {
		let { config, name } = transpile(...source)
		name = camelCase(name)

		if (has(this.nodes, name)) {
			throw new Error(`API already has '${name}' node`)
		}

		this.config[name] = config
		this[name] = createNode(name, config)
		this.nodes.push(name)
	}

	get router() {
		const router = Router()

		router.use(bodyParser.json())

		each(this.nodes, name => {
			router.use(`/${kebabCase(name)}`, this[name].router)
		})

		return router
	}
}
