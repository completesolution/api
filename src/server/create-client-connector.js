import { Router } from 'express'

export default (api) => {
	const router = Router()

	router.get('/get-descriptor', (req, res) => {
		res
			.status(200)
			.json(api.config)
	})

	return { router }
}
