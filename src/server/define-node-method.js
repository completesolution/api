import { camelCase, kebabCase, toUpper, toLower, mapKeys } from 'lodash/fp'
import createAPIResponcesMiddlware from './api-responces-middlware'

const createMethod = (node, config) => {
	const { http_method, method, apiResponcesMiddlware } = config

	return (...args) => {
		const [...middlewares] = args
		const handler = middlewares.pop()

		node.router[toLower(http_method)](
			method,
			apiResponcesMiddlware,
			...middlewares,
			handler
		)

		return node
	}
}

export default (node, config, name) => {
	const { http_method, status_codes, query } = config
	const apiResponcesMiddlware = createAPIResponcesMiddlware(
		query,
		status_codes
	)

	node[camelCase(name)] = createMethod(node, {
		http_method: toUpper(http_method),
		method: `/${kebabCase(name)}`,
		apiResponcesMiddlware
	})
}
