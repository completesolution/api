import { Router } from 'express'
import { each, partial } from 'lodash'
import defineMethod from './define-node-method'

export default (name, methods) => {
	const router = Router()
	const node = { router }

	each(methods, partial(defineMethod, node))

	return node
}
