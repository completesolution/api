import { has } from 'lodash'

export const handle = handlers => (data, ...args) => {
	const { api_status: method } = data

	if (has(handlers, method)) {
		return handlers[method](data, ...args)
	}

	if (has(handlers, 'DEFAULT')) {
		return handlers.DEFAULT(data, ...args)
	}

	return data
}

export const tap = handlers => {
	const handler = handle(handlers)

	return (data, ...args) => {
		handler(data, ...args)
		return data
	}
}
