import {
	isString,
	isNumber,
	keys,
	difference,
	each,
	has
} from 'lodash'

const TypeCheckers = {
	string: isString,
	nubmer: isNumber
}

export default (descriptor, query) => {
	return
	// TODO: Нужно проверять передаваемые данные 
	if (process.env.NODE_ENV === 'development') {
		const desctiptorKeys = keys(descriptor)
		const queryKeys = keys(query)

		const diff = difference(desctiptorKeys, queryKeys)

		if (diff.length > 0) {
			console.warn(`Query does not contain the following fields [${diff}]`)
		}

		each(desctiptorKeys, key => {
			const type = descriptor[key]
			const value = query[key]
			const cheker = TypeCheckers[type]

			if (has(query, key) && !cheker(query[key])) {
				console.warn(`Query attr ${key}(${value}) is not '${type}'`)
			}
		})
	}
}
