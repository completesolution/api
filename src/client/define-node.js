import { camelCase, mapValues, kebabCase, toUpper } from 'lodash'
import { mapKeys } from 'lodash/fp'
import checkQuery from './check-query'
import withQuery from 'with-query'

const createPath = (node, method) => `${kebabCase(node)}/${kebabCase(method)}`

const mapKeysToKebabCase = mapKeys(kebabCase)
const mapKeysToCamelCase = mapKeys(camelCase)

const createMethod = (api, nodeName) => (methodConfig, methodName) => {
	return async (query, options) => {
		checkQuery(methodConfig.query, query)
		query = mapKeysToKebabCase(query)
		const path = createPath(nodeName, methodName)
		
		if (methodConfig.http_method === 'GET') {
			options = {
				...options,
				method: methodConfig.http_method
			}

			return await api
				.fetch(withQuery(path, query), options)
		} else {
			options = {
				...options,
				method: methodConfig.http_method,
				body: JSON.stringify(query)
			}

			return await api
				.fetch(path, options)
		}
	}
}

const createNode = (api, methods, name) => {
	return mapKeysToCamelCase(mapValues(methods, createMethod(api, name)))
}

export default api => (methods, name) => {
	api[camelCase(name)] = createNode(api, methods, name)
}
