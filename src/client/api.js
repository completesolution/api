import { each, merge, isUndefined } from 'lodash'
import defineNode from './define-node'

const defaultConfig = {

	url: {
		protocol: 'http',
		port: 3000,
		host: 'localhost',
		root: '',
		src: false
	},

	options: {
		credentials: 'include',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json;charset=utf-8'
		}
	}

}

export default class API {
	constructor(config) {
		this.config = merge({}, defaultConfig, config)
		this.initFetch() //init fetch by first call
		this.init()
	}

	init() {
		return this.initialization = new Promise(async resolve => {
			const nodes = await this.getNodesDescriptor()
			each(nodes, defineNode(this))
			resolve()
		})
	}

	async getNodesDescriptor() {
		this.descriptor = this.fetch('init/get-descriptor')
		return this.descriptor
	}

	initFetch() {
		const {
			url: { protocol, host, port, root, src },
			options: defaultOptions
		} = this.config

		let prefix = `${protocol}://${host}:${port}/`

		if (src) {
			prefix = '/'
		}

		if (root !== '' && !isUndefined(root)) {
			prefix += `${root}/`
		}

		this.fetch = (path, options) => {
			options = {
				...defaultOptions,
				...options
			}

			return fetch(prefix + path, options)
				.then(json)
		}
	}
}

const json = responce => responce.json()
