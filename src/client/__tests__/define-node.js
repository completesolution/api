import defineNode from '../define-node'
import { put, post } from '../../server/helpers'
import fetch from 'jest-fetch-mock'
import withQuery from 'with-query'

import {
	OK,
	CREATED,
	UNAUTHORIZED,
	FORBIDDEN,
	NOT_FOUND
} from 'http-status'

const mockResponce = {
	http_status: 200,
	api_status: 'ALL_OK',
	data: 'string'
}

const mockNodeConfig = {
	register: put({
		query: { login: 'string', password: 'string', name: 'string', role: 'number' },
		status_codes: {
			USER_REGISTERED		: CREATED,
			USER_ALREADY_EXISTS	: FORBIDDEN
		}
	}),

	authorize: post({
		query: { login: String, password: String },
		status_codes: {
			USER_AUTHORIZED			: CREATED,
			USER_ALREADY_AUTHORIZED	: FORBIDDEN,
			USER_DOES_NOT_EXIST		: NOT_FOUND,
			INCORRECT_PASSWORD		: UNAUTHORIZED
		}
	})
}

const mockAPI = { /* ... */ }

describe('Define client node', () => {

	it('should define node', () => {
		let query

		mockAPI.fetch = jest.fn()
		defineNode(mockAPI)(mockNodeConfig, 'users')

		expect(mockAPI).toHaveProperty('users.register', expect.any(Function))
		expect(mockAPI).toHaveProperty('users.authorize', expect.any(Function))

		query = {
			login: 'putin',
			password: 'crimea',
			name: 'Vladimir',
			role: 'president'
		}

		mockAPI.users.register(query)

		expect(mockAPI.fetch).toBeCalledWith(
			withQuery('users/register', query),
			{ method: 'PUT' }
		)

		query = {
			login: 'putin',
			password: 'crimea'
		}

		mockAPI.users.authorize(query)

		expect(mockAPI.fetch).toBeCalledWith(
			withQuery('users/authorize', query),
			{ method: 'POST' }
		)
	})

})
