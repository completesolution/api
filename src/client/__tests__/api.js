import API from '../api'
import { put, get, post } from '../../server/helpers'

import {
	OK,
	CREATED,
	UNAUTHORIZED,
	FORBIDDEN,
	NOT_FOUND
} from 'http-status'

let api

beforeEach(() => {
	const fetch = jest.fn()
	const root = 'api'

	fetch.mockReturnValueOnce(Promise.resolve({ json: () => ({
		users: {
			register: put({
				query: { login: 'string', password: 'string', name: 'string', role: 'number' },
				status_codes: {
					USER_REGISTERED     : CREATED,
					USER_ALREADY_EXISTS : FORBIDDEN
				}
			}),

			authorize: post({
				query: { login: 'string', password: 'string' },
				status_codes: {
					USER_AUTHORIZED         : CREATED,
					USER_ALREADY_AUTHORIZED : FORBIDDEN,
					USER_DOES_NOT_EXIST     : NOT_FOUND,
					INCORRECT_PASSWORD      : UNAUTHORIZED
				}
			})
		},

		files: {
			rename: post({
				query: { id: 'string', name: 'string' },
				status_codes: {
					FILE_RENAMED   : OK,
					FILE_NOT_FOUND : NOT_FOUND
				}
			}),

			remove: post({
				query: { id: 'string', name: 'string' },
				status_codes: {
					FILE_REMOVED   : OK,
					FILE_NOT_FOUND : NOT_FOUND
				}
			})
		}
	}) }))

	api = new API({
		url: { root },
		fetch
	})
})

describe('Client API', () => {
	it('should work', () => {
		const { any } = expect

		expect(api.initialization).toEqual(any(Promise))

		expect(api).toHaveProperty('users.register', expect.any(Function))
		expect(api).toHaveProperty('users.authorize', expect.any(Function))
		expect(api).toHaveProperty('files.rename', expect.any(Function))
		expect(api).toHaveProperty('files.remove', expect.any(Function))

		api.users.authorize({ login: 'putin', password: 'karelia' })

		expect(api._fetch.mock.calls.length).toBe(2)
		expect(api._fetch.mock.calls[1]).toEqual([
			'http://localhost:3000/api/users/authorize?login=putin&password=karelia',
			{
				'credentials': 'include',
				'headers': {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'POST'
			}
		])
	})
})
