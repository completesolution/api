import { handle, tap } from '../helpers'

describe('client helpers', () => {

	it('should handle', () => {
		const { any } = expect

		const handlerA = jest.fn()
		const handlerB = jest.fn()
		const handlerDEF = jest.fn()

		handlerA.mockReturnValue(42)
		handlerB.mockReturnValue(43)
		handlerDEF.mockReturnValue(44)

		const handler = handle({
			A: handlerA,
			B: handlerB,
			DEFAULT: handlerDEF
		})

		expect(handler).toEqual(any(Function))

		const resultA = handler({ api_status: 'A', data: 'A' })
		const resultB = handler({ api_status: 'B', data: 'B' })
		const resultC = handler({ api_status: 'C', data: 'C' })

		expect(handlerA.mock.calls.length).toBe(1)
		expect(handlerB.mock.calls.length).toBe(1)
		expect(handlerDEF.mock.calls.length).toBe(1)

		expect(resultA).toBe(42)
		expect(resultB).toBe(43)
		expect(resultC).toBe(44)

		expect(handlerA).toBeCalledWith({ api_status: 'A', data: 'A' })
		expect(handlerB).toBeCalledWith({ api_status: 'B', data: 'B' })
		expect(handlerDEF).toBeCalledWith({ api_status: 'C', data: 'C' })
	})

	it('should tap', () => {
		const { any } = expect

		const handlerA = jest.fn()
		const handlerB = jest.fn()
		const handlerDEF = jest.fn()

		handlerA.mockReturnValue(42)
		handlerB.mockReturnValue(43)
		handlerDEF.mockReturnValue(44)

		const handler = tap({
			A: handlerA,
			B: handlerB,
			DEFAULT: handlerDEF
		})

		expect(handler).toEqual(any(Function))

		const resultA = handler({ api_status: 'A', data: 'A' })
		const resultB = handler({ api_status: 'B', data: 'B' })
		const resultC = handler({ api_status: 'C', data: 'C' })

		expect(handlerA.mock.calls.length).toBe(1)
		expect(handlerB.mock.calls.length).toBe(1)
		expect(handlerDEF.mock.calls.length).toBe(1)

		expect(resultA).toEqual({ api_status: 'A', data: 'A' })
		expect(resultB).toEqual({ api_status: 'B', data: 'B' })
		expect(resultC).toEqual({ api_status: 'C', data: 'C' })

		expect(handlerA).toBeCalledWith({ api_status: 'A', data: 'A' })
		expect(handlerB).toBeCalledWith({ api_status: 'B', data: 'B' })
		expect(handlerDEF).toBeCalledWith({ api_status: 'C', data: 'C' })
	})

})
